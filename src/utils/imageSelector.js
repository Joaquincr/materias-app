import defaultImg from '../img/coverItem.png';
import compuToleranteAFallas from '../img/compuToleranteFallas.gif';
import arquiTecguraComputadoras from '../img/ArquitecturaComputadoras.jpg';
import prograParaInternet from '../img/ppi.jpeg';
import traductoresLenguajeUno from '../img/traductoresUno.jpg';

const imageSelector = materia => {
  let image;
  switch (materia) {
    case 'I7024':
      image = arquiTecguraComputadoras;
      break;
    case 'I5909':
      image = prograParaInternet;
      break;
    case 'I7036':
      image = compuToleranteAFallas;
      break;
    case 'I7025':
      image = traductoresLenguajeUno;
      break;
    default:
      image = defaultImg;
  }

  return image;
};
export default imageSelector;
