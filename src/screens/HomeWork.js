import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  Platform,
  ImageBackground,
  RefreshControl,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import UdgHeader from '../components/UdgHeader';
import getHomework from '../APIS/googleApi';
import AsyncStorage from '@react-native-community/async-storage';
import imageSelector from '../utils/imageSelector';

const os = Platform.OS;

let codigo;

const HomeWork = props => {
  FontAwesome.loadFont();
  console.log('props on homework', props);
  let [homeworks, setHomeworks] = useState([]);
  let [isLoading, setIsLoading] = useState(true);
  let [refreshing, setRefreshing] = useState(false);
  let [inFile, setInFile] = useState(true);
  let {crn, nombre_materia, clave_materia = ''} = props;
  let backButton = os === 'ios' ? true : false;

  let image = imageSelector(clave_materia);

  useEffect(() => {
    const getHW = () => {
      let response = getHomework(crn);
      response.then(json => {
        console.log(json);
        if (json.values !== undefined) {
          let finded = findStudent(codigo, json.values);
          if (finded.length <= 0) {
            setInFile(false);
          }
          setHomeworks(finded);
        } else {
          setHomeworks([]);
        }
        setIsLoading(false);
      });
    };

    const getCode = async () => {
      try {
        codigo = await AsyncStorage.getItem('code');
        getHW();
      } catch (e) {
        console.log(e);
      }
    };

    getCode();
  }, [crn]);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    const getHW = () => {
      let response = getHomework(crn);
      response.then(json => {
        if (json.values !== undefined) {
          let finded = findStudent(codigo, json.values);
          setHomeworks(finded);
        } else {
          setHomeworks([]);
        }
        setRefreshing(false);
      });
    };

    const getCode = async () => {
      try {
        codigo = await AsyncStorage.getItem('code');
        getHW();
      } catch (e) {
        console.log(e);
      }
    };

    getCode();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshing]);

  return (
    <SafeAreaView style={styles.container}>
      {/* HEADER */}
      <UdgHeader backButton={backButton} title={'Tareas'} />

      {/* PRINCIPAL CONTAINER */}
      <View style={styles.contentContainer}>
        {/* GENERAL INFO  */}

        {/* LIST OF ITEMS TASKS */}
        <View
          style={
            isLoading
              ? [styles.taskContainer, {justifyContent: 'center'}]
              : styles.taskContainer
          }>
          <FlatList
            ListEmptyComponent={() => {
              return (
                <View style={styles.emptyListContainer}>
                  {isLoading ? (
                    <View>
                      <ActivityIndicator size="large" color="black" />
                      <Text style={styles.taskTextPending}>Cargando...</Text>
                    </View>
                  ) : inFile ? (
                    <Text style={styles.taskTextPending}>
                      No hay tareas para esta materia
                    </Text>
                  ) : (
                    <Text style={styles.taskTextPending}>
                      No estás registado en las tareas de esta materia, contácta
                      a tu maestro para que te registre.
                    </Text>
                  )}
                </View>
              );
            }}
            ListHeaderComponent={
              <ImageBackground
                source={image}
                imageStyle={styles.image}
                style={styles.imageContainer}>
                {/* NOMBRE MATERIA */}
                <View style={styles.headerListContainer}>
                  <Text style={styles.infoContainerText}>{nombre_materia}</Text>
                </View>
                {/* CRN AND CODE*/}
                <View style={styles.infoListContainer}>
                  <Text style={styles.infoContainerText}>{'CRN: ' + crn}</Text>
                  <Text style={styles.infoContainerText}>
                    {'ID: ' + clave_materia}
                  </Text>
                </View>
              </ImageBackground>
            }
            ListHeaderComponentStyle={styles.infoContainer}
            ListFooterComponent={<View style={{height: 25}} />}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            style={styles.list}
            data={homeworks}
            renderItem={({item, i}) => {
              return (
                <CheckBox
                  containerStyle={styles.checkBoxItem}
                  Component={View}
                  title={item.task}
                  checked={item.status === 'TRUE'}
                  checkedColor="#09232f"
                />
              );
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );

  function findStudent(studentCode, tareas) {
    let studentRow;

    tareas.forEach(row => {
      if (row[0] === studentCode) {
        studentRow = row;
      }
    });

    if (!studentRow) {
      return [];
    }

    let tareasFiltradas = tareas[0].filter(v => v !== '');

    let resultado = tareasFiltradas.map((task, index) => {
      console.log(task, index);
      return {
        task: task,
        status: studentRow[index],
      };
    });

    resultado = resultado.filter((v, index) => index > 1);

    return resultado.reverse();
  }
};

export default HomeWork;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red',
  },
  infoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    height: 85,
    marginBottom: 15,
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    height: 85,
    width: '100%',
  },
  headerListContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
  },
  infoListContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
  },
  infoContainerText: {
    color: '#fff',
    fontSize: 14,
    textAlign: 'center',
    textShadowOffset: {width: 0.5, height: 1},
    textShadowColor: 'black',
    textShadowRadius: 1,
    //textTransform: 'capitalize',
  },
  taskContainer: {
    flex: 9,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'lightgrey',
    width: '100%',
  },
  taskItemContainerDone: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgb(0, 177, 124)',
    marginTop: 25,
    padding: 10,
    height: 80,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 1)',
    shadowOffset: {width: 0, height: -1},
    shadowColor: 'rgb(5, 174, 124)',
    shadowOpacity: 0.2,
    elevation: 2.6,
  },

  taskTextDone: {
    color: '#fff',
    fontSize: 16,
    textShadowOffset: {width: -1, height: 0},
    textShadowColor: '#fff',
    textShadowRadius: 0.3,
  },
  taskItemContainerPending: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginTop: 25,
    padding: 10,
    height: 80,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 1)',
    shadowOffset: {width: 0, height: -1},
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 2.6,
  },
  taskTextPending: {
    color: 'rgb(125, 125, 125)',
    fontSize: 16,
    textShadowOffset: {width: 0, height: 0},
    textShadowColor: 'rgb(125, 125, 125)',
    textShadowRadius: 0.3,
  },
  taskItemContainerName: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //ackgroundColor: 'grey',
    padding: 10,
  },
  taskItemContainerStatus: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'lightgrey',
    padding: 10,
  },
  list: {
    width: '100%',
    backgroundColor: 'white',
  },
  emptyListContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 600,
    padding: 25,
    backgroundColor: 'white',
  },
  image: {
    width: '100%',
    resizeMode: 'cover',
    opacity: 0.3,
  },
  checkBoxItem: {
    marginLeft: 15,
    marginRight: 15,
  },
});
