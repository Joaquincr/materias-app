import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  Alert,
  Image,
  Platform,
  StatusBar,
  Text,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Input, Button} from 'react-native-elements';
import getStudentInfoAndMatters from '../APIS/udgApi';
import AsyncStorage from '@react-native-community/async-storage';
import KeyboardListener from 'react-native-keyboard-listener';
import {StudentContext} from '../contexts/studentContext';
import Icon from 'react-native-vector-icons/AntDesign';

const {width} = Dimensions.get('window');
const os = Platform.OS;

import logo from '../img/logoudg.png';

const Login = () => {
  Icon.loadFont();

  let [code, setCode] = useState('');
  let [nip, setNip] = useState('');
  let [isLoading, setIsLoading] = useState(false);
  let [isKeyboarOpen, setIsKeyboardOpen] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#09232f" barStyle="default" />
      {/* KEYBOARD HANDLER */}
      <KeyboardListener
        onWillShow={() => setIsKeyboardOpen(true)}
        onWillHide={() => setIsKeyboardOpen(false)}
        onDidShow={() => setIsKeyboardOpen(true)}
        onDidHide={() => setIsKeyboardOpen(false)}
      />

      {/* LOGO CONTAINER */}
      {!isKeyboarOpen && (
        <View style={styles.logoContainer}>
          <Image style={styles.logo} source={logo} />
        </View>
      )}

      {/* FORM CONTAINER */}
      <View style={styles.formContainer}>
        <Input
          containerStyle={styles.inputs}
          label="Código"
          leftIcon={{type: 'antdesign', name: 'user', color: '#09232f'}}
          leftIconContainerStyle={styles.iconInputCustomStyle}
          labelStyle={styles.inputLabel}
          placeholder="Tu código de estudiante"
          onChangeText={text => {
            setCode(text);
          }}
          value={code}
          keyboardType={'number-pad'}
          disabled={isLoading}
          selectionColor={isKeyboarOpen && os === 'ios' ? '#09232f' : false}
        />
        <Input
          containerStyle={styles.inputs}
          label="Nip"
          placeholder="Tu NIP"
          onChangeText={text => {
            setNip(text);
          }}
          value={nip}
          autoCapitalize={'none'}
          secureTextEntry={true}
          disabled={isLoading}
          labelStyle={styles.inputLabel}
          leftIcon={{type: 'antdesign', name: 'lock', color: '#09232f'}}
          leftIconContainerStyle={styles.iconInputCustomStyle}
          selectionColor={isKeyboarOpen && os === 'ios' ? '#09232f' : false}
        />
        <View style={styles.formContainerFooter}>
          <Text style={styles.formContainerFooterText}>
            Usa las credenciales con las que entras a SIIAU
          </Text>
        </View>
      </View>

      {/* BUTTONS CONTAINER */}
      <View
        style={
          isKeyboarOpen && os === 'ios'
            ? [styles.buttonsContainer, {justifyContent: 'flex-start', flex: 6}]
            : styles.buttonsContainer
        }>
        <StudentContext.Consumer>
          {context => (
            <Button
              title="Iniciar Sesión"
              containerStyle={styles.customButtonContainerStyle}
              buttonStyle={styles.customButtonStyle}
              onPress={() => loginHandler(context.updateStudent)}
              loading={isLoading}
              disabled={isLoading}
            />
          )}
        </StudentContext.Consumer>
      </View>
    </SafeAreaView>
  );

  /**
   * Make petition to get student information
   * and set flags to loaders an error messages.
   */
  async function loginHandler(updateStudent) {
    setIsLoading(true);
    let response = getStudentInfoAndMatters(code, nip);
    let student = await response;

    if (student === 'Sesion invalida') {
      Alert.alert(
        'Upps..',
        'Las credenciales no son correctas, intentalo de nuevo',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: true},
      );
      setIsLoading(false);
    } else if (student.alumno === 'No se encontro el alumno') {
      Alert.alert(
        'Error',
        'No se encontro el alumno',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: true},
      );
      setIsLoading(false);
    } else {
      setIsLoading(false);
      setStudent(student);
      console.log('update student from login', student);
      updateStudent(student);
      Actions.home(student);
    }
  }

  /**
   * Persist the user info on the AsyncStorage
   * @param {string} student
   */
  async function setStudent(student) {
    try {
      await AsyncStorage.setItem('student', JSON.stringify(student));
      await AsyncStorage.setItem('code', code);
      await AsyncStorage.setItem('nip', nip);
    } catch (e) {
      console.log(e);
    }
  }
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logoContainer: {
    flex: 4.5,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 25,
    paddingTop: 55,
    width: '100%',
    //backgroundColor: 'grey',
  },
  logo: {
    width: 180,
    height: 250,
  },
  formContainer: {
    flex: 4.5,
    width: width,
    //backgroundColor: 'lightgrey',
    paddingLeft: 25,
    paddingRight: 25,
  },
  formContainerFooter: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    //backgroundColor: 'black',
    marginTop: 25,
  },
  formContainerFooterText: {
    color: 'grey',
    fontSize: 12,
  },
  inputs: {
    marginTop: 40,
    //backgroundColor: 'red',
  },
  inputLabel: {
    color: '#09232f',
  },
  iconInputCustomStyle: {
    //backgroundColor: 'blue',
    marginLeft: 0,
    //marginRight: 5,
    paddingRight: 10,
  },
  buttonsContainer: {
    flex: 3,
    width: width,
    padding: 25,
    //backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
  },
  customButtonContainerStyle: {
    width: width - 70,
  },
  customButtonStyle: {
    backgroundColor: '#09232f',
  },
});
