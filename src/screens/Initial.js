import React from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import useAuthUser from '../hooks/useAuthUser';
import {Actions} from 'react-native-router-flux';

const Initial = () => {
  useAuthUser(false) ? Actions.home() : Actions.login();
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color="black" />
      <Text>Cargando...</Text>
    </View>
  );
};
export default Initial;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
