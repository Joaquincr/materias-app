import React from 'react';
import {View, StatusBar, SafeAreaView, StyleSheet} from 'react-native';
import UdgHeader from '../components/UdgHeader';
import {ListItem} from 'react-native-elements';

const Profile = props => {
  let {alumno, campus, carrera, ciclo} = props;
  console.log(props);

  const list = [
    {
      name: 'Nombre',
      subtitle: alumno,
    },
    {
      name: 'Campus',
      subtitle: campus,
    },
    {
      name: 'Carrera',
      subtitle: carrera,
    },
    {
      name: 'Ciclo',
      subtitle: ciclo,
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#09232f" barStyle="default" />
      <UdgHeader drawerButton title={'Mi Perfil'} />
      <View style={styles.contentContainer}>
        {list.map((l, i) => (
          <ListItem
            style={styles.itemContainer}
            key={i}
            title={l.name}
            titleStyle={{color: '#09232f'}}
            subtitle={l.subtitle}
            subtitleStyle={{color: 'grey'}}
            bottomDivider
          />
        ))}
      </View>
    </SafeAreaView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#fff'},
  contentContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  itemContainer: {
    width: '100%',
  },
});
