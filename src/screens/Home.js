import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  FlatList,
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import CardClass from '../components/CardClass';
import useStudent from '../hooks/useStudent';
import UdgHeader from '../components/UdgHeader';

let {width} = Dimensions.get('window');

const Home = props => {
  console.log(props);

  let [student, setStudent] = useState({});
  let responseStudent = useStudent(student);
  let {horario} = responseStudent;

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#09232f" barStyle="default" />
      <UdgHeader drawerButton title={'Materias'} />
      <View style={styles.itemsContainer}>
        <FlatList
          style={styles.list}
          data={horario}
          renderItem={({item, i}) => <CardClass materia={item} />}
          ListFooterComponent={<View style={{height: 25}} />}
        />
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#fff'},
  itemsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '100%',
  },
  badgesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'lightgrey',
    width: width - 50,
  },
  list: {
    width: '100%',
  },
});
