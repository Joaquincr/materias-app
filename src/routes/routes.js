import React from 'react';
import {Router, Stack, Scene, Drawer, Actions} from 'react-native-router-flux';
import Login from '../screens/Login';
import Home from '../screens/Home';
import Profile from '../screens/Profile';
import UdgDrawer from '../components/UdgDrawer';
import Initial from '../screens/Initial';
import HomeWork from '../screens/HomeWork';
import StudenContextProvider from '../contexts/studentContext';

const App = () => (
  <StudenContextProvider>
    <Router>
      <Drawer
        hideNavBar
        key="drawer"
        onOpen={() => {
          console.log('open drawer');
        }}
        onExit={() => {
          console.log('Drawer closed');
        }}
        onEnter={() => {
          Actions.refresh();
          console.log('Drawer enter');
        }}
        onLeft={() => {
          console.log('on left drawer');
        }}
        contentComponent={UdgDrawer}
        drawerWidth={300}>
        <Stack key="drawerroot">
          <Scene
            key="initial"
            component={Initial}
            title="initial"
            type="reset"
            initial
            hideNavBar
          />
          <Scene
            key="home"
            component={Home}
            title="Home"
            type="reset"
            hideNavBar
          />
          <Scene
            key="profile"
            component={Profile}
            title="Profile"
            type="reset"
            hideNavBar
          />
          <Scene
            key="homework"
            component={HomeWork}
            title="Home Work"
            hideNavBar
          />

          <Scene
            key="login"
            component={Login}
            titile="Login"
            type="reset"
            drawerLockMode={'locked-closed'}
            hideNavBar
          />
        </Stack>
      </Drawer>
    </Router>
  </StudenContextProvider>
);

export default App;
