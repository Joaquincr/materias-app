import React, {Component, createContext} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export let StudentContext = createContext();

const getStudent = async () => {
  try {
    const asyncStudent = await AsyncStorage.getItem('student');
    console.log('student at async: ', asyncStudent);
    return asyncStudent;
  } catch (e) {
    console.log(e);
  }
};

class StudenContextProvider extends Component {
  state = {
    student: {name: 'sdafa'},
    updateStudent: student => {
      console.log('update student', student);
      this.setState({student: student});
    },
  };

  componentDidMount() {
    getStudent().then(content => {
      let json = JSON.parse(content);
      console.log('student at effect', json);
      this.setState({student: json});
    });
  }

  render() {
    return (
      <StudentContext.Provider value={this.state}>
        {this.props.children}
      </StudentContext.Provider>
    );
  }
}

export default StudenContextProvider;
