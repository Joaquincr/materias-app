import axios from 'axios';

const getStudentInfoAndMatters = async (code, nip) => {
  const formData = new FormData();
  formData.append('codigo', code);
  formData.append('nip', nip);

  const response = await axios.post(
    'http://148.202.152.33/ws_alumno.php',
    formData,
  );
  return response.data;
};

export default getStudentInfoAndMatters;
