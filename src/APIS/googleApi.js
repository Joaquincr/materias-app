import axios from 'axios';
import * as googleKeys from '../key/googleKeys.json';
import * as spreadsheetsKeys from '../key/gapi.json';

const getHomework = async subjectCode => {
  console.log(googleKeys.api_key);
  //let endpoint = `https://sheets.googleapis.com/v4/spreadsheets/1PlyvE-QN9GFMeP1LP7InqDxTjDV5buUpW5VJyzaum_s/values/${subjectCode}!A1:Z1000?key=${googleKeys.api_key}`;
  let endpoint = `https://sheets.googleapis.com/v4/spreadsheets/${
    spreadsheetsKeys.spreadsheet
  }/values/${subjectCode}!A1:Z1000?key=${googleKeys.api_key}`;
  console.log(endpoint);
  let response = await axios.get(endpoint, {
    validateStatus: function(status) {
      return status < 500; // Reject only if the status code is greater than or equal to 500
    },
  });
  console.log(response.data);
  return response.data;
};

export default getHomework;
