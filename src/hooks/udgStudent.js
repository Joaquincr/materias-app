import {useState, useEffect} from 'react';
import getStudentInfoAndMatters from '../APIS/udgApi';

const useUdgStudent = props => {
  const [student, setStudent] = useState({});
  const {code, nip} = props;

  console.log(code, nip);

  useEffect(() => {
    let response = getStudentInfoAndMatters(code, nip);

    response.then(json => {
      //console.log('response on promisse', json);
      setStudent(json);
    });

    return () => {
      console.log('unmonting component');
    };
  }, [code, nip]); //render only if the properties code or nip have a change.

  return student;
};

export default useUdgStudent;
