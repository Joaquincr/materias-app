import {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

const useAuthUser = props => {
  const [auth, setAuth] = useState();
  const {authorized} = props;

  useEffect(() => {
    getStudent();
    return () => {
      console.log('unmonting component');
    };
  }, [authorized]); //render only if the properties code or nip have a change.

  const getStudent = async () => {
    try {
      const student = await AsyncStorage.getItem('student');
      student !== null ? setAuth(true) : setAuth(false);
      console.log('student', student);
    } catch (e) {
      console.log(e);
    }
  };

  return auth;
};

export default useAuthUser;
