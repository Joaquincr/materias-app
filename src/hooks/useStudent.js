import {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

const useStudent = actualStudent => {
  const [student, setStudent] = useState({});

  useEffect(() => {
    getStudent().then(content => {
      let json = JSON.parse(content);
      console.log('student at effect', json);
      setStudent(json);
    });

    return () => {
      console.log('unmonting component');
    };
  }, [actualStudent]); //render only if the properties code or nip have a change.

  const getStudent = async () => {
    try {
      const asyncStudent = await AsyncStorage.getItem('student');
      console.log('student at async: ', asyncStudent);
      return asyncStudent;
    } catch (e) {
      console.log(e);
    }
  };

  return student;
};

export default useStudent;
