import React from 'react';
import {StyleSheet, View, Text, SafeAreaView} from 'react-native';
import {ListItem} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/AntDesign';
import IconA from 'react-native-vector-icons/Ionicons';
import MaterialIconts from 'react-native-vector-icons/MaterialIcons';
import {StudentContext} from '../../contexts/studentContext';

const UdgDrawer = props => {
  Icon.loadFont();
  IconA.loadFont();
  MaterialIconts.loadFont();

  let student = {alumno: '', carrera: '', ciclo: ''};

  const tabs = [
    {
      title: 'Perfil',
      icon: 'user',
      onPress: () => {
        Actions.currentScene === 'profile'
          ? Actions.drawerClose()
          : Actions.profile(student);
      },
    },
    {
      title: 'Materias',
      icon: 'book',
      onPress: () => {
        Actions.currentScene === 'home'
          ? Actions.drawerClose()
          : Actions.home();
      },
    },
    {
      title: 'Cerrar Sesión',
      icon: 'poweroff',
      onPress: () => {
        AsyncStorage.clear();
        Actions.login();
      },
    },
  ];

  return (
    <StudentContext.Consumer>
      {context => {
        if (context && context.student) {
          student = context.student;
        }

        let {alumno = '', carrera = '', ciclo = ''} = student;
        console.log('Udg drawer context', context);
        return (
          <SafeAreaView style={styles.container}>
            {Actions.currentScene === 'login' ? (
              <View style={styles.emptyContainer}>
                <Text>Inicia Sesión</Text>
              </View>
            ) : (
              <View style={styles.controlsContainer}>
                <View style={styles.userInfoContainer}>
                  <ListItem
                    title={alumno}
                    subtitle={`${carrera} - ${ciclo}`}
                    bottomDivider
                    topDivider
                    style={styles.studentInfoitem}
                    titleStyle={{color: '#09232f'}}
                    subtitleStyle={{color: 'grey'}}
                  />
                </View>
                <View style={styles.tabsContainer}>
                  {tabs.map((item, i) => (
                    <ListItem
                      key={i}
                      title={item.title}
                      leftIcon={{
                        name: item.icon,
                        type: 'antdesign',
                        color: 'grey',
                      }}
                      style={styles.items}
                      titleStyle={{color: '#09232f'}}
                      onPress={item.onPress}
                      bottomDivider
                      chevron
                    />
                  ))}
                </View>
              </View>
            )}
          </SafeAreaView>
        );
      }}
    </StudentContext.Consumer>
  );
};

export default UdgDrawer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  userInfoContainer: {
    backgroundColor: 'black',
    marginBottom: 10,
  },
  tabsContainer: {
    justifyContent: 'flex-start',
  },
  studentInfoitem: {
    width: '100%',
    // shadowOffset: {width: 0, height: 0},
    // shadowColor: 'black',
    // shadowOpacity: 0.2,
  },
  items: {
    // shadowOffset: {width: 0, height: 1},
    // shadowColor: 'black',
    // shadowOpacity: 0.2,
    // marginBottom: 2,
  },
  controlsContainer: {
    backgroundColor: 'lightgrey',
  },
});
