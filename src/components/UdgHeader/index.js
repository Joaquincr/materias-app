import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';

const UdgHeader = props => {
  let {title, backButton, drawerButton} = props;
  return (
    <View style={styles.container}>
      {drawerButton && (
        <Icon
          name="menu-fold"
          type="antdesign"
          color="#09232f"
          onPress={() => {
            Actions.drawerOpen();
          }}
        />
      )}

      {backButton && (
        <Icon
          name="left"
          type="antdesign"
          color="#09232f"
          onPress={() => {
            Actions.pop();
          }}
        />
      )}

      <Text style={styles.titleText}>{title}</Text>
      {/* <Button title="LogOut" onPress={_logoutHandler} /> */}
    </View>
  );
};

export default UdgHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 0.75,
    borderBottomColor: 'lightgrey',
    padding: 10,
    elevation: 3,
    width: '100%',
  },
  titleText: {
    color: '#09232f',
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'capitalize',
  },
});
