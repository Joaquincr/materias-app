import React from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import {Text, Icon} from 'react-native-elements';
import imageSelector from '../../utils/imageSelector';

import {Actions} from 'react-native-router-flux';

import AntDesignIcons from 'react-native-vector-icons/AntDesign';

const CardClass = props => {
  AntDesignIcons.loadFont();

  let {materia} = props;
  let image = imageSelector(materia.clave_materia);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Actions.homework(materia);
      }}
      style={styles.container}>
      <View style={styles.container}>
        <ImageBackground
          imageStyle={styles.image}
          source={image}
          style={styles.headerCardContainer}>
          <View style={styles.headerCardInfo}>
            <Text style={styles.materiaText}>{materia.nombre_materia}</Text>
            <Text style={styles.maestroText}>{materia.profesor}</Text>
          </View>
        </ImageBackground>
        <View style={styles.contentCardContainer}>
          <View style={styles.contentCardItemContainer}>
            <Icon
              name="calendar"
              type="antdesign"
              iconStyle={styles.contentCardIcon}
            />
            <Text style={styles.contentText}>{materia.dias} </Text>
          </View>

          <View style={styles.contentCardItemContainer}>
            <Icon
              name="enviromento"
              type="antdesign"
              iconStyle={styles.contentCardIcon}
            />
            <Text style={styles.contentText}>
              {materia.edificio} {materia.aula}
            </Text>
          </View>
          <View style={styles.contentCardItemContainer}>
            <Icon
              name="clockcircleo"
              type="antdesign"
              iconStyle={styles.contentCardIcon}
            />
            <Text style={styles.contentText}>{materia.horario}</Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CardClass;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    marginTop: 15,
    borderColor: 'rgba(217, 217, 217, 1)',
    height: 200,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    borderWidth: 1,
    shadowOffset: {width: 0, height: -1},
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 2,
  },
  headerCardContainer: {
    flex: 5,
    backgroundColor: '#000',
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomColor: 'rgba(217, 217, 217, 1)',
    borderBottomWidth: 1,
  },
  headerCardInfo: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'transparent',
    height: '100%',
    padding: 15,
  },
  contentCardContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    width: '100%',
    padding: 15,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  contentCardItemContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: 'red',
  },
  contentCardIcon: {
    fontSize: 14,
    marginRight: 5,
    color: '#09232f',
  },
  image: {
    resizeMode: 'cover',
    opacity: 0.3,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  materiaText: {
    color: '#fff',
    fontSize: 18,
    textShadowOffset: {width: 1, height: 1},
    textShadowColor: '#fff',
    textShadowRadius: 1,
    //textTransform: 'capitalize',
  },

  maestroText: {
    color: '#fff',
    fontSize: 11,
    textTransform: 'capitalize',
  },
  contentText: {
    color: '#09232f',
  },
});
